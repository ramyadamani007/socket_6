//								SERVER SIDE 								//

/*********************
	Include
*********************/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<sys/un.h>

// Main //

int main ()
{
	// Declaration of variables //
	// x = Server, y = client //
	
	int x,y;
	int x_len, y_len;
	struct sockaddr_un server_address;
	struct sockaddr_un client_address;
	
	// removing old sockets and creating an unnamed socket.//
	
	unlink ("server_socket");
	x = socket (AF_UNIX, SOCK_STREAM, 0);
	
	// Naming the Socket //
	
	server_address.sun_family = AF_UNIX;
	strcpy (server_address.sun_path, "server_socket");
	x_len = sizeof (server_address);
	bind (x, (struct sockaddr *)&server_address, x_len);
	
	// Creating connection queue and waiting for clients.//
	
	listen (x, 5);
	while (1)
	{
		char ch;
		printf("Server waiting...\n");
		
		// Accepting a connection. //
		
		y_len = sizeof (client_address);
		y = accept (x, (struct sockaddr *)&client_address, &y_len);
		
		// Reading and Writing on client_sockfd //
		
		read(y, &ch, 1);
		ch++;
		write(y, &ch, 1);
		close(y);
	}
	printf("\n");
	return 0;
}
		
	
	
	
	
	
	
	

//								SERVER SIDE								//

/******************
	Include
******************/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<sys/un.h>
#include<arpa/inet.h>
#include<netinet/in.h>

# define PORT 2749

// MAIN //

int main ()
{
	int ser_socket, res;
	struct sockaddr_in server_address;
	
	int new_socket;
	struct sockaddr_in new_address;
	
	char buf [512];
	pid_t childpid;
	
	ser_socket = socket (AF_INET, SOCK_STREAM, 0);
	if (ser_socket<0)
	{
		printf("Error in connection.\n");
		exit (1);
	}
	printf("server socket is created.\n");
	
	memset (&server_address, '\0', sizeof (server_address));
	
	server_address.sin_family = AF_INET;
	server_address.sin_port = htons (PORT);
	server_address.sin_addr.s_addr = inet_addr("127.0.0.1");
	
	
	res = bind(ser_socket, (struct sockaddr*)&server_address, sizeof (server_address));
	if (res<0)
	{
		printf("Error in Binding.\n");
		exit(1);
	}
	printf("Bind to part %d\n", 2749);
	
	if (listen(ser_socket, 4) == 0)
	{
		printf("Listening...\n");
	}
	else 
	{
		printf("Error in binding.\n");
	}
	
	while (1)
	{
		new_socket = accept (ser_socket, (struct sockaddr*)&new_address, sizeof (new_address));
		if (new_socket <0)
		{
			exit (1);
		}
		printf("Connection accepted from %s: %d\n", inet_ntoa(new_address.sin_addr), htons(new_address.sin_port));
		
		if ((childpid = fork()) == 0)
		{
			close (ser_socket);
			while (1)
			{
				recv (new_socket, buf, 512, 0);
				if (strcmp(buf, "exit") == 0)
				{
					printf("disconnected from server%s: %d\n", inet_ntoa(new_address.sin_addr), htons(new_address.sin_port));
					break;
				}
				else 
				{
					printf("client: %s\n",buf);
					send (new_socket, buf, strlen (buf), 0);
					bzero(buf, sizeof (buf));
				}
			}
		}
		
	}
	close (new_socket);
	printf("\n");
	return 0;
}
	
	
	
	
	

	

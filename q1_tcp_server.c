//								SERVER SIDE								//

/*********************
	Include
*********************/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<sys/un.h>
#include<arpa/inet.h>
#include<netinet/in.h>

// Main //

int main ()
{
	char ser_msg[256] = "You have reached the server! Now, you can send/recieve the data.";
	int ser_socket;
	ser_socket = socket (AF_INET, SOCK_STREAM, 0);
	
	struct sockaddr_in server_address;
	server_address.sin_family = AF_INET;
	server_address.sin_port = htons (9002);
	
	server_address.sin_addr.s_addr = INADDR_ANY;
	bind (ser_socket, (struct sockaddr *) &server_address, sizeof (server_address));
	listen (ser_socket, 4);
	int cli_socket;
	cli_socket = accept (ser_socket, (struct sockaddr *) &server_address, sizeof (server_address));
	
	send (cli_socket, ser_msg, sizeof(ser_msg), 0);
	close (ser_socket);
	printf("\n");
	return 0;
}

//								CLIENT SIDE								//

/*********************
	Include
*********************/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<sys/un.h>
#include<arpa/inet.h>
#include<netinet/in.h>

// Main //

int main ()
{
	int net_socket;
	
	// Calling socket function //
	// storing function descriptor in integer //
	
	net_socket = socket (AF_INET, SOCK_STREAM, 0);
	
	// specifying address of socket //
	
	struct sockaddr_in server_address;
	server_address.sin_family = AF_INET;
	
	// specify the port we want to connect //
	server_address.sin_port = htons (9002);
	
	// specifying actual address //
	server_address.sin_addr.s_addr = INADDR_ANY;
	
	// connect fuction to connect another end //
	int a = connect (net_socket, (struct sockaddr *) &server_address, sizeof (server_address));
	
	if (a == 0)
	{
		printf("Connection successfull.\n");
	}
	else 
	{
		printf("No connection established.\n");
	}
	
	char ser_resp[256];
	recv (net_socket, &ser_resp, sizeof (ser_resp), 0);
	printf("The server sent the data that is = %s\n",ser_resp);
	close(socket);
	return 0;
}



	
	
	
	
	

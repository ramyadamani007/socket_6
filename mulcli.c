//								CLIENT SIDE								//

/******************
	Include
******************/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<sys/un.h>
#include<arpa/inet.h>
#include<netinet/in.h>

# define PORT 2749

// MAIN //

int main ()
{
	int cli_socket, res;
	struct sockaddr_in server_address;
	char ch [512];
	
	cli_socket = socket (AF_INET, SOCK_STREAM, 0);
	
	if (cli_socket < 0)
	{
		printf("Error in connection.\n");
		exit (1);
	}
	printf("Client socket is created.\n");
	
	memset (&server_address, '\0', sizeof (server_address));
	
	server_address.sin_family = AF_INET;
	server_address.sin_port = htons (PORT);
	server_address.sin_addr.s_addr = inet_addr("127.0.0.1");
	res = connect (cli_socket, (struct sockaddr*)&server_address, sizeof (server_address));
	
	if (res<0)
	{
		printf("Error in connection.\n");
		exit (1);
	}
	printf("Connection succesfully established.\n");
	
	while (1)
	{
		printf("Client: \t");
		scanf("%s",&ch[0]);
		send (cli_socket, ch, strlen (ch), 0);
		
		if (strcmp(ch, "exit") == 0)
		{
			printf("disconnected from server.\n");
			exit (1);
		}
		
		if (recv(cli_socket, ch, 512, 0))
		{
			printf("Error in recieving data.\n");
			exit (1);
		}
		else 
		{
			printf("server: \t %s \n, ch");
		}
	}
	return 0;
}
	
	
	
	
	
	
	
	
	
